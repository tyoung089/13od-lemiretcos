# This is the README for the data of Heather Mirletz's (nee Lemire) data from her masters thesis.

## Degradation of Transparent Conductive Oxides; Mechanistic observations and interfacial engineering
 
## Author: Heather Lemire
### Thesis Advisor, R. H. French
### Mentor, I. T. Martin
### Faculty, A. Sehirlioglu
#[Case Western Reserve University, SDLE Center, MORE Center] [1]
#http://rave.ohiolink.edu/etdc/view?acc_num=case1386325661

#This 13ms-LemireTCOs data is made available under the Open Data Commons Attribution License: http://opendatacommons.org/licenses/by/{version}. - See more at: http://opendatacommons.org/licenses/by/#sthash.hdnOEDcX.dpuf
